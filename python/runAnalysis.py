#! /usr/bin/env python
import shutil, os, logging, re, sys
from datetime import datetime
import argparse
if __name__ == "__main__":
    import ROOT
    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
    ROOT.xAOD.Init().ignore();
    AnalysisHelp = ROOT.MariageOfDisaster.SUSYAnalysisHelper("AnalysisHelper")
    AnalysisHelp.initialize().isSuccess()
 
