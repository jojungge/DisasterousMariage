// IAnalysisHelper.h
#ifndef DisasterousMariage_IAnalysisHelper_H
#define DisasterousMariage_IAnalysisHelper_H

#include "AsgTools/IAsgTool.h"
#include <DisasterousMariage/Defs.h>
namespace CP {
    class SystematicSet;
}

namespace MariageOfDisaster {
    class IAnalysisHelper: virtual public asg::IAsgTool {
            ASG_TOOL_INTERFACE (IAnalysisHelper)

        public:
    
            virtual StatusCode finalize()=0;

          
            
    };
}
#endif
